--------------------------------------------------------------------------------
--
--   FileName:         average.vhd
--   Dependencies:     none
--   Design Software:  Quartus II 64-bit Version 13.1 Full Version
-- 
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
--use work.global.all;
use work.fixed_point_adder;
use work.qdiv;
use std.textio.all;

entity average is
	generic (N : natural := 48; Q : natural := 0);
	port (
		-- Clock.
		a_clk				: in std_logic;
		-- Reset.
		a_rst				: in std_logic;
		
		-- Input port.
		data_valid			: in std_logic;
		data_in_X			: in std_logic_vector(15 downto 0);
		data_in_Y			: in std_logic_vector(15 downto 0);
		data_in_Z			: in std_logic_vector(15 downto 0);
		
		-- Output port.
		available			: out std_logic;
		data_valid_cnf			: out std_logic;
		data_valid_cnf_addition		: out std_logic;
		data_out_addition		: out std_logic_vector(N-1 downto 0);
		data_out			: out std_logic_vector(N-1 downto 0)
	);
end entity average;

architecture arch_average of average is
	-- Clock.
	signal clk					: std_logic;
	-- Reset.
	signal rst					: std_logic;
	
	-- Type of states in state machine
	type state_type is (restart,s0,s1,s2,s3,s4);  	
	-- current and next state declaration	
	signal current_s, next_s			: state_type;  	
	
	-- Data for fixed-point addition (3)
	signal r1, r2, r3				: std_logic_vector(N-1 downto 0);
	signal R_No					: std_logic_vector(15 downto 0);
	signal r1_tmp, r2_tmp, r3_tmp	 		: std_logic_vector(N-1 downto 0);
	
	signal data, result, division_result		: std_logic_vector(N-1 downto 0);
	
	-- Division signals.
	signal division_complete, division_overflow 	: std_logic;
	-- Data valid signals.
	signal dv, dv1, dv2, dv3			: std_logic;
	
	signal countInputLatency			: std_logic_vector(4 downto 0); -- 2^5 = 32
	signal countDivlatency				: std_logic_vector(7 downto 0); -- 2^8 = 256
	
	signal shift_reg 				: std_logic_vector(N+Q-1 downto 0);	

	-- For the println :)
	file stdout: text open write_mode is "STD_OUTPUT";
	procedure println(s: string) is
		variable l: line;
	begin
		write(l, s);
		writeline(stdout, l);
	end procedure println; 
begin

	R_No <= "0000000000000011"; --number of registers inside the Average Block (curr. 3)
	clk <= a_clk;
	rst <= a_rst;
	
	---
	data(N-1 downto 2*N/3) <= data_in_X;	--highest bits are for X
	data(2*N/3-1 downto N/3) <= data_in_Y;
	data(N/3-1 downto 0) <= data_in_Z;	--lowest bits are for Z
	---
	
	process (clk, rst)
	begin
		if (rst='0') then
			current_s <= restart;	--default state on reset		
		elsif (rising_edge(clk)) then
			current_s <= next_s;	--state change
		end if;
	end process;	

	dv <= data_valid; 

	
	---------------------------------------------------
	--- 		State Machine Process		---
	---------------------------------------------------
	process (current_s, data)
	begin
		case current_s is

  			when restart =>

 				r1	<= (others => '0');	
 				r2	<= (others => '0');	
 				r3	<= (others => '0');
				dv1	<= '0';
				countDivlatency <= (others => '0');
				next_s  <= s0;
	
			when s0 =>			--when current state is "s0"
				countDivlatency <= (others => '0');
				if (data_valid='1') then	--if data valid is 1
					r1 <= data;			--write input into R1 and
					next_s <= s1;			--change the state
				else				--if data valid is NOT 1
					next_s <= s0;			--stay in the same state
				end if;
			
			when s1 => 			--when current state is "s1"
				if (data_valid='1') then
					r2 <= data;
					next_s <= s2;
				else
					next_s <= s1;
				end if;
			
			when s2 =>			--when current state is "s2"
				if (data_valid='1') then
					r3 <= data;
					next_s <= s3;
					dv1 <= '1';	--???check
				else
					next_s <= s2;
				end if;
			
			when s3 => 			--when current state is "s3"
				dv1 <= '0';		--???check				
				next_s <= s4;

			when s4 => 			--when current state is "s4"
				countDivlatency <= countDivlatency + 1;					
				next_s <= s3; 		-- should be s4?				
				--if(countDivlatency = N+Q) then
				if(countDivlatency = 13) then 
					next_s <= s0;
				end if;
		
		end case;
	end process;

	-- shift register
    	process (clk, rst)
    	begin
		if (rst='0') then
			shift_reg <= (others => '0');
		elsif (rising_edge(clk)) then
			shift_reg(28) <= dv1;
			for I in 28 downto 1 loop
		    		shift_reg(I-1) <= shift_reg(I);
			end loop;
		end if;
    	end process;

	-- data available for wrining into R1, R2, R3
    	process (clk, rst)
    	begin
		if (rst='0') then
			available <= '0';
			countInputLatency <= (others => '0');
		elsif (rising_edge(clk)) then
			if (dv1='1') then --pipelining started (starts with adders)
				available <= '0'; 
				countInputLatency <= "11100"; --28
			elsif (countInputLatency > 0) then
				available <= '0'; --now he can read next data inputs
				countInputLatency <= countInputLatency-1;
			else
				available <= '1';
				countInputLatency <= (others => '0');
			end if;
		end if;
    	end process;	

	process (clk, rst)
	begin
		if (rst='0') then
			dv2 <= '0';
                        r2_tmp <= (others => '0');
                        r3_tmp <= (others => '0'); 

		elsif (rising_edge(clk)) then
			dv2 <= dv1;			--propagate data value
			r2_tmp <= r1_tmp;
                        r3_tmp <= r3;
		end if;
	end process;

	process (clk, rst)
	begin
		if (rst='0') then
			data_out <= (others => '0');
			data_out_addition <= (others => '0');
		elsif (rising_edge(clk)) then
			data_valid_cnf_addition <= dv2; --propagate data value
			data_valid_cnf <= shift_reg(0);
			data_out <= division_result; 
			data_out_addition <= result; 
		end if;
	end process;
	

	---------------------------------------------------
	--- 		X Fixed-Point Adder		---
	---------------------------------------------------
	--1st pair: fixed-point ADDITION
	fixed_point_adder_x_1: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r1(N-1 downto 2*N/3),
		i_B	=> r2(N-1 downto 2*N/3),
		
		o_C    	=> r1_tmp(N-1 downto 2*N/3)
	);
	
	--2nd pair: fixed-point ADDITION
	fixed_point_adder_x_2: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r2_tmp(N-1 downto 2*N/3),
		i_B	=> r3_tmp(N-1 downto 2*N/3),
		
		o_C    	=> result(N-1 downto 2*N/3)
	);

	---------------------------------------------------
	--- 		Y Fixed-Point Adder		---
	---------------------------------------------------
	--1st pair: fixed-point ADDITION
	fixed_point_adder_y_1: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r1(2*N/3-1 downto N/3),
		i_B	=> r2(2*N/3-1 downto N/3),
		
		o_C    	=> r1_tmp(2*N/3-1 downto N/3)
	);
	
	--2nd pair: fixed-point ADDITION
	fixed_point_adder_y_2: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r2_tmp(2*N/3-1 downto N/3),
		i_B	=> r3_tmp(2*N/3-1 downto N/3),
		
		o_C    	=> result(2*N/3-1 downto N/3)
	);

	---------------------------------------------------
	--- 		Z Fixed-Point Adder		---
	---------------------------------------------------
	--1st pair: fixed-point ADDITION
	fixed_point_adder_z_1: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r1(N/3-1 downto 0),
		i_B	=> r2(N/3-1 downto 0),
		
		o_C    	=> r1_tmp(N/3-1 downto 0)
	);
	
	--2nd pair: fixed-point ADDITION
	fixed_point_adder_z_2: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r2_tmp(N/3-1 downto 0),
		i_B	=> r3_tmp(N/3-1 downto 0),
		
		o_C    	=> result(N/3-1 downto 0)
	);
	
	---------------------------------------------------
	--- 		Fixed-Point Division X		---
	---------------------------------------------------
	fixed_point_divider_X: entity qdiv
	port map(
		i_clk  	=> clk,
		i_start => rst,
		
		i_dividend => result(N-1 downto 2*N/3),
		i_divisor => R_No,
		
		o_quotient_out => division_result(N-1 downto 2*N/3),
		o_complete => division_complete,
		o_overflow => division_overflow
	);

	---------------------------------------------------
	--- 		Fixed-Point Division Y		---
	---------------------------------------------------
	fixed_point_divider_Y: entity qdiv
	port map(
		i_clk  	=> clk,
		i_start => rst,
		
		i_dividend => result(2*N/3-1 downto N/3),
		i_divisor => R_No,
		
		o_quotient_out => division_result(2*N/3-1 downto N/3),
		o_complete => division_complete,
		o_overflow => division_overflow
	);

	---------------------------------------------------
	--- 		Fixed-Point Division Z		---
	---------------------------------------------------
	fixed_point_divider_Z: entity qdiv
	port map(
		i_clk  	=> clk,
		i_start => rst,
		
		i_dividend => result(N/3-1 downto 0),
		i_divisor => R_No,
		
		o_quotient_out => division_result(N/3-1 downto 0),
		o_complete => division_complete,
		o_overflow => division_overflow
	);
	
	
end arch_average;
