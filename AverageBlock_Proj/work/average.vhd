--------------------------------------------------------------------------------
--
--   FileName:         average.vhd
--   Dependencies:     none
--   Design Software:  Quartus II 64-bit Version 13.1 Full Version
-- 
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
--use work.global.all;
use work.fixed_point_adder;
use work.qdiv;

entity average is
	generic (N : natural := 48);
	port (
		-- Clock.
		a_clk				: in std_logic;
		-- Reset.
		a_rst				: in std_logic;
		
		-- Input port.
		data_valid			: in std_logic;
		data_in				: in std_logic_vector(N-1 downto 0);
		
		-- Output port.
		data_valid_cnf			: out std_logic;
		data_out			: out std_logic_vector(N-1 downto 0)
	);
end entity average;

architecture arch_average of average is
	-- Clock.
	signal clk					: std_logic;
	-- Reset.
	signal rst					: std_logic;
	
	-- Type of states in state machine
	type state_type is (restart,s0,s1,s2,s3);  	
	-- current and next state declaration	
	signal current_s, next_s			: state_type;  	
	
	-- Data for fixed-point addition (3)
	signal r1, r2, r3, R_No				: std_logic_vector(N-1 downto 0);
	signal r1_tmp, r2_tmp, r3_tmp	 		: std_logic_vector(N-1 downto 0);
	
	signal data, result, division_result		: std_logic_vector(N-1 downto 0);
	
	-- Division signals.
	signal division_complete, division_overflow 	: std_logic;
	-- Data valid signals.
	signal dv, dv1, dv2, dv3			: std_logic;
begin
	R_No <= "000000000000000000000000000000000000000000000011";
	clk <= a_clk;
	rst <= a_rst;

	data <= data_in;
	
	process (clk, rst)
	begin
		if (rst='0') then
			current_s <= restart;	--default state on reset		
		elsif (rising_edge(clk)) then
			current_s <= next_s;	--state change
		end if;
	end process;	

	dv <= data_valid; 
	--data_valid_cnf <= dv3;
	--state machine process
	process (current_s, data)
	begin
		case current_s is

  			when restart =>

 				r1	<= (others => '0');	
 				r2	<= (others => '0');	
 				r3	<= (others => '0');
				dv1	<= '0';
				next_s  <= s0;
	
			when s0 =>			--when current state is "s0"
				if (data_valid='1') then		--if data valid is 1
					r1 <= data;			--write input into R1 and
					next_s <= s1;			--change the state
				else				--if data valid is NOT 1
					next_s <= s0;			--stay in the same state
				end if;
			
			when s1 => 			--when current state is "s1"
				if (data_valid='1') then
					r2 <= data;
					next_s <= s2;
				else
					next_s <= s1;
				end if;
			
			when s2 =>			--when current state is "s2"
				if (data_valid='1') then
					r3 <= data;
					next_s <= s3;
					dv1 <= '1';	--???check
				else
					next_s <= s2;
				end if;
			
			when s3 => 			--when current state is "s3"
				dv1 <= '0';		--???check				
				next_s <= s0;
		end case;
	end process;
	--end state machine process
	
	process (clk, rst)
	begin
		if (rst='0') then
			dv2 <= '0';
                        r2_tmp <= (others => '0');
                        r3_tmp <= (others => '0'); 

	--	elsif (rising_edge(clk) and next_s=s3) then
		elsif (rising_edge(clk)) then
			dv2 <= dv1;	--propagate data value
			r2_tmp <= r1_tmp;--------------------------------------------
                        r3_tmp <= r3;
		end if;
	end process;

	process (clk, rst)
	begin
		if (rst='0') then
			--dv3 <= '0';
			data_out <= (others => '0');
			--data_valid_cnf <= '0';

--		elsif (rising_edge(clk) and next_s=s3) then
		elsif (rising_edge(clk)) then
			--dv3 <= dv2;	--propagate data value
			data_valid_cnf <= dv2;
			--data_valid_cnf <= division_complete;
			data_out <= division_result; 
			--data_valid_cnf <= dv3;
		end if;
	end process;
	
	--data_valid_cnf <= dv3;

	--1st pair: fixed-point ADDITION
	fixed_point_adder_1: entity fixed_point_adder
	generic map (
		N => N
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r1,
		i_B	=> r2,
		
		o_C    	=> r1_tmp
	);
	
	--2nd pair: fixed-point ADDITION
	fixed_point_adder_2: entity fixed_point_adder
	generic map (
		N => N
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r2_tmp,
		i_B	=> r3_tmp,
		
		o_C    	=> result
	);
	
	--pair: fixed-point DIVISION
	fixed_point_divider: entity qdiv
	port map(
		i_clk  	=> clk,
		i_start => rst,
		
		i_dividend => result,
		i_divisor => R_No,
		
		o_quotient_out => division_result,
		o_complete => division_complete,
		o_overflow => division_overflow
	);
	
	
end arch_average;
