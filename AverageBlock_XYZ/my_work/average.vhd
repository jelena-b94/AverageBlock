--------------------------------------------------------------------------------
--
--   FileName:         average.vhd
--   Dependencies:     none
--   Design Software:  Quartus II 64-bit Version 13.1 Full Version
-- 
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use my_work.global.all;
use my_work.fixed_point_adder;

entity average is
	generic (N : natural := 47);
	port (
		-- Clock.
		a_clk				: in std_logic;
		-- Reset.
		a_rst				: in std_logic;
		
		-- Input port.
		data_valid		: in std_logic;
		data_in			: in std_logic_vector(N downto 0);
		
		-- Output port.
		data_valid_cnf	: out std_logic;
		data_out			: out std_logic_vector(N downto 0)
	);
end entity average;

architecture arch_average of average is
	-- Clock.
	signal clk						: std_logic;
	-- Reset.
	signal rst						: std_logic;
	
	-- Type of state machine (on the picture there are s0, s1, s2)
	type state_type is (s0,s1,s2,s3);  	
	-- current and next state declaration	
	signal current_s, next_s	: state_type;  	
	
	-- Data for fixed-point addition (3)
	signal r1, r2, r3 			: std_logic_vector(N downto 0);
	signal r_temp1, r_temp2		: std_logic_vector(N downto 0);
	
	signal data, result			: std_logic_vector(N downto 0);
	
	-- Data valid signals.
	signal dv, dv1, dv2, dv3	: std_logic;
begin
	
	clk <= a_clk;
	rst <= a_rst;

	data <= data_in;
	
	process (clk, rst)
	begin
		if (rst='1') then
			current_s <= s0;		--default state on reset		
		elsif (rising_edge(clk)) then
			current_s <= next_s;	--state change
		end if;
	end process;	

	dv <= data_valid; 
	
	-- ???? U if je dv1 uvek?
	--state machine process
	process (current_s, data)
	begin
		case current_s is 
			when s0 =>			--when current state is "s0"
				if (dv='1') then		--if data valid is 1
					r1 <= data;				--write input into R1 and
					next_s <= s1;			--change the state
				else						--if data valid is NOT 1
					next_s <= s0;			--stay in the same state
				end if;
			
			when s1 => 			--when current state is "s1"
				if (dv='1') then
					r2 <= r1;
					next_s <= s2;
				else
					next_s <= s1;
				end if;
			
			when s2 =>			--when current state is "s2"
				if (dv='1') then
					r3 <= r2;
					next_s <= s3;
					dv1 <= dv;	--???check
				else
					next_s <= s2;
				end if;
			
			when s3 => 			--when current state is "s3"
				dv1 <= '0';		--???check
				next_s <= s0;
		end case;
	end process;
	--end state machine process
	
	
	--1st pair: fixed-point addition
	fixed_point_adder_1: entity fixed_point_adder
	generic map (
		N => 48
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A		=> r1,
		i_B		=> r2,
		
		o_C    	=> r_temp1
	);
	
	process (clk, rst)
	begin
		if (rst='1') then
			dv2 <= '0';
		elsif (rising_edge(clk)) then
			dv2 <= dv1;	--propagate data value
			r_temp2 <= r_temp1;
		end if;
	end process;
	
	--2nd pair: fixed-point addition
	fixed_point_adder_2: entity fixed_point_adder
	generic map (
		N => 48
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A		=> r_temp2,
		i_B		=> r3,
		
		o_C    	=> result
	);
	
	process (clk, rst)
	begin
		if (rst='1') then
			dv3 <= '0';
		elsif (rising_edge(clk)) then
			dv3 <= dv2;	--propagate data value
			data_out <= result; 
			data_valid_cnf <= dv3;
		end if;
	end process;
	
	
end arch_average;
