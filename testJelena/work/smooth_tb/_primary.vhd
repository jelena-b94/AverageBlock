library verilog;
use verilog.vl_types.all;
entity smooth_tb is
    generic(
        widthSort       : integer := 32
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of widthSort : constant is 1;
end smooth_tb;
