library verilog;
use verilog.vl_types.all;
entity stim_gen is
    port(
        clk             : in     vl_logic;
        enable          : in     vl_logic;
        data            : out    vl_logic_vector(47 downto 0);
        data_valid      : out    vl_logic
    );
end stim_gen;
