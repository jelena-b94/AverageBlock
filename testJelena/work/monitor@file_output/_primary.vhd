library verilog;
use verilog.vl_types.all;
entity monitorFile_output is
    port(
        clk             : in     vl_logic;
        enable          : in     vl_logic;
        mode            : in     vl_logic;
        dataToMon       : in     vl_logic_vector(31 downto 0)
    );
end monitorFile_output;
