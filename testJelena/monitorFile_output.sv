module monitorFile_output (
  clk,
  enable,
  mode,
  dataToMon
  );

  input wire clk;
  input wire enable;
  input wire mode;
  input wire [31:0] dataToMon;
  
  
  integer f,i;

  //Clock and reset release
  initial begin

  end

  initial begin
  

	f = $fopen("output.txt","w");  

    @(posedge enable); //Wait for enable

    while (1'b1) begin
      @(posedge clk);

      $fwrite(f,"%f \t %h \t %b \n", $bitstoshortreal(dataToMon),dataToMon,dataToMon);
    end

    $fclose(f);  

    $finish;
  end
  
endmodule