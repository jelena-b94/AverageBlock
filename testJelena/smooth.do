##################################
# A very simple modelsim do file #
##################################

# 1) Create a library for working in
vlib work
vlib mti_lib

vmap work work

# 2) Compile the afu_float
#vlog -L altera_mf_ver -L lpm_ver fpMult.v
vlog stim_gen.sv
vlog monitorFile_input.sv
vlog monitorFile_output.sv
vlog smooth.sv
vcom global.vhd
vcom fixed_point_adder.vhd
vcom average.vhd
vlog smooth_tb.sv


# 3) Load it for simulation
vsim -novopt -L altera_mf_ver -L lpm_ver smooth_tb


# 4) Open some selected windows for viewing
view structure
view signals
view wave

# 5) Show some of the signals in the wave window
add wave -noupdate -divider -height 32 Inputs
add wave -noupdate clk
add wave -noupdate reset_n
add wave -noupdate -hex data_in
add wave -noupdate data_valid
add wave -noupdate newData
add wave -noupdate -decimal countlength
add wave -noupdate -divider -height 32 Outputs
add wave -noupdate -hex data_out
add wave -noupdate data_valid_cnf


# 6) Set some test patterns
run 500000 ns 

wave zoomfull
