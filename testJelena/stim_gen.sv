module stim_gen (
   // Input
   clk,  
   enable,
   // Outputs
   data,
   data_valid
   );
   
   input wire clk;
   input wire enable;
   output reg [47:0] data;
   output reg data_valid;   

   integer code;

   reg[1:0] data_tmp;
   
   initial begin

      data = 0;
    
      data_tmp = 0;
      code = 1;
     
	  
    @(posedge enable); //Wait for enable
    @(posedge clk);
	
      //$monitor("data = %x", data);
      while (code) begin

	     data_tmp = data_tmp+1;
             data     = data + 1;


         @(posedge clk);
      end
      $finish;
   end // initial begin

   assign data_valid = data_tmp[0];

endmodule // stim_gen
