#!/bin/tcsh
setenv LM_LICENSE_FILE 1800@lxlicen01,1800@lxlicen02,1800@lxlicen03:/home/cfaerber/qpifpga/SR-4.1.7/Base/HW/qpi-qxp/license.dat
setenv MGLS_LICENSE_FILE 1717@lxlicen01,1717@lxlicen02,1717@lxlicen03,1717@lnxmics1,1717@lxlicen08
setenv CDS_LIC_FILE 2000@lxlicen01,2000@lxlicen02,2000@lxlicen03

	vlib lpm_ver
	vlib altera_mf_ver	
	vmap lpm_ver lpm_ver
	vmap altera_mf_ver altera_mf_ver
	vlog -work altera_mf_ver /opt/altera/13.1/quartus/eda/sim_lib/altera_mf.v
	vlog -work lpm_ver /opt/altera/13.1/quartus/eda/sim_lib/220model.v

r\m -r -f work

vlib work
vmap work work
#vlog -L altera_mf_ver -L lpm_ver fpMult.v
vlog stim_gen.sv
vlog monitorFile_input.sv
vlog monitorFile_output.sv
vlog smooth.sv
vcom global.vhd
vcom fixed_point_adder.vhd
vcom average.vhd
vlog smooth_tb.sv


#vsim -L altera_mf_ver -L lpm_ver afu_cqrt_tb

vsim -do smooth.do -L altera_mf_ver -L lpm_ver smooth_tb 
