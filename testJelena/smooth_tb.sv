//-------------------------------------------------
// File: afu_mandel.sv
// Purpose: System Verilog Simulation Example
//          Test Bench
//-----------------------------------------------------------

`timescale 1 ns /  100 ps

module smooth_tb ();
  // PARAMETERS
     parameter widthSort = 32;
  //---------------------------------------------------------
  // inputs to the DUT are reg type
     reg clk;
     reg reset_n;
     reg newData;
     wire [47:0] data_in;
     wire data_valid;
     reg [ 8:0] countlength;
  //--------------------------------------------------------
  // outputs from the DUT are wire type
     wire [47:0] data_out;
	 wire data_valid_cnf;

  //---------------------------------------------------------
  // instantiate the Device Under Test (DUT)
  // using named instantiation



   
   
// Stimuli from File
stim_gen stim_gen_inst1(
   // Input
   .clk( clk ),
   .enable( newData ),
   // Outputs
   .data( data_in ),
   .data_valid( data_valid )
   );
  
  
// Monitor Input to File
monitorFile_input monitorFile_inst1(
  //Input
  .clk ( clk ),
  .enable ( newData ),
  .mode(1'b0),
  .dataToMon ( data_in )
  ); 


  
average average_inst1(
                // Clock.
                .a_clk 			( clk ),
                // Reset.
                .a_rst 			( ~reset_n ),
                // Input port.
                .data_valid 	( data_valid ),
                .data_in 		( data_in ),
                // Output port.
                .data_valid_cnf ( data_valid_cnf ),
                .data_out 		( data_out )
                );



  //----------------------------------------------------------
  // create a 200Mhz clock
  always
    #2.5 clk = ~clk;   // every ten nanoseconds invert


  always @(posedge clk) begin
     countlength  <= countlength +1; 
  end
  //-----------------------------------------------------------
  // initial blocks are sequential and start at time 0
  initial
  begin
    $display($time, " << Starting the Simulation >>");
    clk = 1'b0;                    
    countlength = 0;                                  
// at time 0
    reset_n = 1'b0;
    newData = 1'b0;                                                

    #20

    reset_n = 1'b1; 
   
    @(negedge clk);
    $display($time, " << Turning ON the sort enable >>");
    newData = 1'b1;
    $display($time, " << Simulation Complete >>");

  end

 
endmodule //of smooth
