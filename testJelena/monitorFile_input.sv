module monitorFile_input (
  clk,
  enable,
  mode,
  dataToMon,
  );

  input wire clk;
  input wire enable;
  input wire mode;
  input wire [47:0] dataToMon;
  
  
  integer f,i;

  //Clock and reset release
  initial begin

  end

  initial begin
  

	f = $fopen("input.txt","w");  

    @(posedge enable); //Wait for enable

    while (1'b1) begin
      @(posedge clk);

      $fwrite(f,"%d \t %h \t %b \n", dataToMon,dataToMon,dataToMon);
    end

    $fclose(f);  

    $finish;
  end
  
endmodule
