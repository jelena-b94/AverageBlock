--------------------------------------------------------------------------------
--
--   FileName:         average.vhd
--   Dependencies:     none
--   Design Software:  Quartus II 64-bit Version 13.1 Full Version
-- 
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
--use work.global.all;
use work.fixed_point_adder;
use work.qdiv;

entity average is
	generic (N : natural := 48);
	port (
		-- Clock.
		a_clk				: in std_logic;
		-- Reset.
		a_rst				: in std_logic;
		
		-- Input port.
		data_valid			: in std_logic;
		data_in_X			: in std_logic_vector(15 downto 0);
		data_in_Y			: in std_logic_vector(15 downto 0);
		data_in_Z			: in std_logic_vector(15 downto 0);
		
		-- Output port.
		data_valid_cnf			: out std_logic;
		data_out			: out std_logic_vector(N-1 downto 0)
	);
end entity average;

architecture arch_average of average is
	-- Clock.
	signal clk					: std_logic;
	-- Reset.
	signal rst					: std_logic;
	
	-- Type of states in state machine.
	type state_type is (restart,s0,s1,s2,s3);  	
	-- current and next state declaration	
	signal current_s, next_s			: state_type;  	
	
	-- Data for fixed-point addition (3x2).
	signal r1_X, r1_Y, r1_Z				: std_logic_vector(15 downto 0);
	signal r2_X, r2_Y, r2_Z				: std_logic_vector(15 downto 0);
	signal r3_X, r3_Y, r3_Z				: std_logic_vector(15 downto 0);
	signal r1_X_tmp, r1_Y_tmp, r1_Z_tmp		: std_logic_vector(15 downto 0);
	signal r2_X_tmp, r2_Y_tmp, r2_Z_tmp		: std_logic_vector(15 downto 0);
	signal r3_X_tmp, r3_Y_tmp, r3_Z_tmp		: std_logic_vector(15 downto 0);

	-- Input data (X,Y,Z 16-bit).
	signal data_X, data_y, data_Z			: std_logic_vector(15 downto 0);
	
	-- Addition.
	signal result 					: std_logic_vector(N-1 downto 0);
	--signal result_X 				: std_logic_vector(15 downto 0);
	--signal result_Y 				: std_logic_vector(15 downto 0);
	--signal result_Z 				: std_logic_vector(15 downto 0);
	
	-- Divisor (number of registers). 
	signal R_No					: std_logic_vector(N-1 downto 0);
	-- Division signals.
	signal division_complete, division_overflow 	: std_logic;
	-- Division result
	signal division_result				: std_logic_vector(N-1 downto 0);
	
	-- Data valid signals.
	signal dv, dv1, dv2				: std_logic;
begin
	R_No <= "000000000000000000000000000000000000000000000011";
		
	clk <= a_clk;
	rst <= a_rst;

	---
	data_X <= data_in_X;
	data_Y <= data_in_Y;
	data_Z <= data_in_Z;
	---
	
	process (clk, rst)
	begin
		if (rst='0') then
			current_s <= restart;	--default state on reset		
		elsif (rising_edge(clk)) then
			current_s <= next_s;	--state change
		end if;
	end process;	

	dv <= data_valid; 

	
	---------------------------------------------------
	--- 		State Machine Process		---
	---------------------------------------------------
	process (current_s, data_X, data_Y, data_Z)
	begin
		case current_s is

  			when restart =>

 				r1_X	<= (others => '0');	
				r1_Y	<= (others => '0');	
				r1_Z	<= (others => '0');	
 				
				r2_X	<= (others => '0');
				r2_Y	<= (others => '0');
				r2_Z	<= (others => '0');	
 				
				r3_X	<= (others => '0');
				r3_Y	<= (others => '0');
				r3_Z	<= (others => '0');
				dv1	<= '0';
				next_s  <= s0;
	
			when s0 =>			--when current state is "s0"
				if (data_valid='1') then		--if data valid is 1
					r1_X <= data_X;			--write input into R1 and
					r1_Y <= data_Y;
					r1_Z <= data_Z;
					next_s <= s1;			--change the state
				else				--if data valid is NOT 1
					next_s <= s0;			--stay in the same state
				end if;
			
			when s1 => 			--when current state is "s1"
				if (data_valid='1') then
					r2_X <= data_X;
					r2_Y <= data_Y;
					r2_Z <= data_Z;
					next_s <= s2;
				else
					next_s <= s1;
				end if;
			
			when s2 =>			--when current state is "s2"
				if (data_valid='1') then
					r3_X <= data_X;
					r3_Y <= data_Y;
					r3_Z <= data_Z;
					next_s <= s3;
					dv1 <= '1';	--???check
				else
					next_s <= s2;
				end if;
			
			when s3 => 			--when current state is "s3"
				dv1 <= '0';		--???check				
				next_s <= s0;
		end case;
	end process;

	
	process (clk, rst)
	begin
		if (rst='0') then
			dv2 <= '0';
                        r2_X_tmp <= (others => '0');
                        r2_Y_tmp <= (others => '0');
                        r2_Z_tmp <= (others => '0');
                        r3_X_tmp <= (others => '0'); 
                        r3_Y_tmp <= (others => '0');
                        r3_Z_tmp <= (others => '0');

		elsif (rising_edge(clk)) then
			dv2 <= dv1;	--propagate data value
			r2_X_tmp <= r1_X_tmp;
			r2_Y_tmp <= r1_Y_tmp;
			r2_Z_tmp <= r1_Z_tmp;
                        r3_X_tmp <= r3_X;
                        r3_Y_tmp <= r3_Y;
                        r3_Z_tmp <= r3_Z;
		end if;
	end process;

	process (clk, rst)
	begin
		if (rst='0') then
			data_out <= (others => '0');
		elsif (rising_edge(clk)) then
			data_valid_cnf <= dv2;
			--data_valid_cnf <= division_complete;
			--data_out <= division_result; 
			data_out <= result; 
		end if;
	end process;
	
	---------------------------------------------------
	--- 		X Fixed-Point Adder		---
	---------------------------------------------------
	--1st pair: fixed-point ADDITION
	fixed_point_adder_x_1: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r1_X,
		i_B	=> r2_X,
		
		o_C    	=> r1_X_tmp
	);
	
	--2nd pair: fixed-point ADDITION
	fixed_point_adder_x_2: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r2_X_tmp,
		i_B	=> r3_X_tmp,
		
		o_C    	=> result(N-1 downto 2*N/3)
	);

	---------------------------------------------------
	--- 		Y Fixed-Point Adder		---
	---------------------------------------------------
	--1st pair: fixed-point ADDITION
	fixed_point_adder_y_1: entity fixed_point_adder
	generic map (
		N => N/3
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r1_Y,
		i_B	=> r2_Y,
		
		o_C    	=> r1_Y_tmp
	);
	
	--2nd pair: fixed-point ADDITION
	fixed_point_adder_y_2: entity fixed_point_adder
	generic map (
		N => 16
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r2_Y_tmp,
		i_B	=> r3_Y_tmp,
		
		o_C    	=> result(2*N/3-1 downto N/3)
	);

	---------------------------------------------------
	--- 		Z Fixed-Point Adder		---
	---------------------------------------------------
	--1st pair: fixed-point ADDITION
	fixed_point_adder_z_1: entity fixed_point_adder
	generic map (
		N => 16
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r1_Z,
		i_B	=> r2_Z,
		
		o_C    	=> r1_Z_tmp
	);
	
	--2nd pair: fixed-point ADDITION
	fixed_point_adder_z_2: entity fixed_point_adder
	generic map (
		N => 16
	)
	port map(
		i_clk  	=> clk,
		i_rst 	=> rst,
		
		i_A	=> r2_Z_tmp,
		i_B	=> r3_Z_tmp,
		
		o_C    	=> result(N/3-1 downto 0)
	);
	
	---------------------------------------------------
	--- 		Fixed-Point Division		---
	---------------------------------------------------
	fixed_point_divider: entity qdiv
	port map(
		i_clk  	=> clk,
		i_start => rst,
		
		i_dividend => result,
		i_divisor => R_No,
		
		o_quotient_out => division_result,
		o_complete => division_complete,
		o_overflow => division_overflow
	);
	
	
end arch_average;
